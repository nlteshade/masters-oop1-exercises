package InputOutput;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputOutput {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int age = getAge();
        System.out.println("The user says their age is: " +age);
        age = getAge("Please Input your age");
        System.out.println("The user says their age is: " +age);
        age = getAge("Please Input your age", "John");
        System.out.println("John says his age is: " +age);
        if(checkPension(47)) {
            System.out.println("This person is able to collect a pension");
        }
        else {
            System.out.println("This person is NOT able to collect a pension");
        }
        if(checkPension(87)) {
            System.out.println("This person is able to collect a pension");
        }
        else {
            System.out.println("This person is NOT able to collect a pension");
        }
        String name = getName();
        System.out.println("The Users name is " +name);
        String[] names = new String[2];
        getName(names);
        for(int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }
    }

    private static String getName(){
        System.out.println("Please enter your Name: ");
        String name = sc.nextLine();
        return name;
    }

    private static void getName(String[] name) {
        System.out.println("Please enter your Name: ");
        name[0] = sc.next();
        name[1] = sc.next();
    }

    private static int getAge() {
        System.out.println("Please enter your Age: ");
        int age = sc.nextInt();
        return age;
    }

    private static int getAge(String input) {
        System.out.println(input);
        int age = sc.nextInt();
        return age;
    }

    private static int getAge(String input, String name) {
        System.out.println("Hi " +name +" " +input);
        int age = 0;
        try{
            age = sc.nextInt();
        }catch(InputMismatchException e) {
            age = -999;
            e.printStackTrace();
        }
        return age;
    }

    private static boolean checkPension(int age) {
        if(age >= 65) {
           return true;
        }
        else {
            return false;
        }
    }
}
