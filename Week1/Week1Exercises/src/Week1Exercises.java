import java.util.Scanner;

public class Week1Exercises {

    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Week1Exercises week1Exercises = new Week1Exercises();
        if(didOswaldActAlone()) {
            System.out.println("the User believes Oswald Acted alone");
        }
        else {
            System.out.println("the User believes Oswald DID NOT Act alone");
        }
        if(week1Exercises.wasOjGuilty()) {
            System.out.println("the User believes OJ was Guilty");
        }
        else{
            System.out.println("the User believes OJ was NOT Guilty");
        }
        String favFilm = favouriteFilm();
        System.out.println("Users Favourite Film is: " +favFilm);
        int filmRating = filmRating(favFilm);
        System.out.println("User gave " +favFilm+" a rating of: " +filmRating);
    }

    private static boolean didOswaldActAlone() {
        System.out.println("Do you believe Oswald acted alone? Y/N: ");
        if (sc.next().equalsIgnoreCase("Y")) {
            return true;
        } else {
            return false;
        }
    }

    private boolean wasOjGuilty(){
        System.out.println("Do you believe OJ eas Guilty? Y/N: ");
        if (sc.next().equalsIgnoreCase("Y")) {
            return true;
        } else {
            return false;
        }
    }

    private static String favouriteFilm(){
        System.out.println("What is your Favourite Film?: ");
        return sc.next();
    }

    private static int filmRating(String favFilm){
        System.out.println("Rate " +favFilm +" from 1-10: ");
        return sc.nextInt();
    }

}
