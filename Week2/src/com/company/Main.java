package com.company;

import java.util.Scanner;

enum Direction{
    NORTH, SOUTH, EAST, WEST
}

public class Main {

    private static  Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        preAndPostDifference();
        booleanLogicShortCircuitOps();
        booleanBitwiseOps();
        compareStrings();
        System.out.print("Certification for movie ");
        int certification = sc.nextInt();
        System.out.print("Enter age for entry ");
        int age = sc.nextInt();
        admitToFilm(certification, age);
        ternaryOperator();
        switchVowelOrConsonant();
        ifMonth();
        ifGrade();

    }

    public static void preAndPostDifference(){
        int x = 5, y = 10;
        System.out.println("The value of x is " + x);
        System.out.println("The value of ++x is " + ++x );
        System.out.println("The value of x++ is " + x++ );
        System.out.println("The value of x is " + x);

        System.out.println("The value of x is " + y);
        System.out.println("The value of ++x is " + --y);
        System.out.println("The value of x++ is " + y--);
        System.out.println("The value of x is " + y);
    }

    public static void booleanLogicShortCircuitOps(){

        System.out.print("Enter num1 ");
        int num1 = sc.nextInt();
        System.out.print("Enter num2 ");
        int num2 = sc.nextInt();

        if(num1 < 0 && num2++ < 0){
            System.out.println(num2);
        }
        else if(num1 > 0 && num2++ > 0) {
            System.out.println(num2);
        }
        else if (num1 == 0 || num2++ == 0) {
            System.out.println(num2);
        }
        else if (num1 < 0 || num2++ < 0) {
            System.out.println(num2);
        }

        System.out.println(num2);

    }

    public static void booleanBitwiseOps(){

        System.out.print("Enter num1 ");
        int num1 = sc.nextInt();
        System.out.print("Enter num2 ");
        int num2 = sc.nextInt();

        if(num1 < 0 & num2++ < 0){
            System.out.println(num2);
        }
        else if(num1 > 0 & num2++ > 0) {
            System.out.println(num2);
        }
        else if (num1 == 0 | num2++ == 0) {
            System.out.println(num2);
        }
        else if (num1 < 0 | num2++ < 0) {
            System.out.println(num2);
        }

        System.out.println(num2);
    }

    public static void compareStrings(){

        System.out.println("Enter s1 ");
        String s1 = sc.next();
        System.out.println("Enter s2 ");
        String s2 = sc.next();

        System.out.println("Using == returns " + s1 == s2);
        System.out.println("Using equals() returns " + s1.equals(s2));

        String name1 = "Sean";
        String name2 = "Sean";

        System.out.println("Using == returns " + name1 == name2);

        String name3 = "John";
        String name4 = new String("John");

        System.out.println("Using == returns " + name3 == name4);



    }

    public static boolean admitToFilm(int certification, int age){
        if(age >= certification) {
            return true;
        }
        else {
            return false;
        }
    }

    public static void ternaryOperator(){
        boolean isHappy = true;
        String mood = "I am happy!";

        if(!isHappy){
            mood = "I am NOT happy!";
        }

        System.out.print("Enter x ");
        int x = sc.nextInt();
        System.out.print("Enter y ");
        int y = sc.nextInt();
        int minVal = x;

        if(x > y){
            minVal = y;
        }

        System.out.println("Min Val is " +minVal);

    }

    public static void switchVowelOrConsonant() {
        System.out.println("Please input a character");
        char letter = sc.next().charAt(0);

        switch(letter) {
            case 'A':
            case 'a':
            case 'E':
            case 'e':
            case 'I':
            case 'i':
            case 'O':
            case 'o':
            case 'U':
            case 'u':
                System.out.println("is a vowel");
                break;
            default:
                System.out.println("is a consonant");
        }

        switch(letter) {
            case (int) 'A':
            case 'a':
            case (int) 'E':
            case 'e':
            case (int) 'I':
            case 'i':
            case (int) 'O':
            case 'o':
            case (int) 'U':
            case 'u':
                System.out.println("is a vowel");
                break;
            default:
                System.out.println("is a consonant");
        }

        switch(letter) {
            default:
                System.out.println("is a consonant");
            case (int) 'A':
            case 'a':
            case (int) 'E':
            case 'e':
            case (int) 'I':
            case 'i':
            case (int) 'O':
            case 'o':
            case (int) 'U':
            case 'u':
                System.out.println("is a vowel");
                break;
        }

    }

    public static void ifMonth(){
        final int JAN=1, FEB=2, MAR=3, APR=4,
        MAY=5, JUN=6, JUL=7, AUG=8, SEPT=9, OCT=10,
        NOV=11, DEC=12;

        try {
            System.out.println("Please enter a month 1-12");
            int month = sc.nextInt();

            if (month == JAN) {
                System.out.println(month + " is JAN");
            } else if (month == FEB) {
                System.out.println(month + " is FEB");
            } else if (month == MAR) {
                System.out.println(month + " is MAR");
            } else if (month == APR) {
                System.out.println(month + " is APR");
            } else if (month == MAY) {
                System.out.println(month + " is MAY");
            } else if (month == JUN) {
                System.out.println(month + " is JUN");
            } else if (month == JUL) {
                System.out.println(month + " is JUL");
            } else if (month == AUG) {
                System.out.println(month + " is AUG");
            } else if (month == SEPT) {
                System.out.println(month + " is SEPT");
            } else if (month == OCT) {
                System.out.println(month + " is OCT");
            } else if (month == NOV) {
                System.out.println(month + " is NOV");
            } else if (month == DEC) {
                System.out.println(month + " is DEC");
            } else {
                System.out.println(month + " is out of range");
            }
        }catch (Exception e) {
            System.out.println("Exception Caught");
            e.printStackTrace(System.out);
        }
    }

    public static void ifGrade(){
        System.out.println("Please enter a Grade 0-100");
        int grade = sc.nextInt();

        try{
            if(grade<= 100 && grade>= 70){
                System.out.println("A");
            }
            else if(grade<= 69 && grade>= 60){
                System.out.println("B");
            }
            else if(grade<= 59 && grade>= 50){
                System.out.println("C");
            }
            else if(grade<= 49 && grade>= 40){
                System.out.println("D");
            }
            else{
                System.out.println("Fail");
            }
        }
        catch (Exception e){
            System.out.println("Exception Caught");
            e.printStackTrace(System.out);
        }


    }

    public static void switchMathOperation(){
        double answer = 0.0;
        boolean charOK = true;

        System.out.println("Please enter a double");
        double num1 = sc.nextDouble();
        System.out.println("Please enter a another double");
        double num2 = sc.nextDouble();
        System.out.println("Please enter a Character");
        char operation = sc.next().charAt(0);

        switch (operation){
            case '+':
                answer = num1 + num2;
                break;
            case '-':
                answer = num2 - num1;
                break;
            case '*':
                answer = num2 * num1;
                break;
            case '/':
                answer = num1 / num2;
                break;
            default:
                System.out.println("Unknown mathematical operator " + operation);
                charOK = false;
        }

        if (charOK)
            System.out.println("Answer is " + answer);

    }

    public static void switchEnumType(){

        Direction theWay = null;

        System.out.println("Please enter a direction (N, S, E or W)");
        String direction = sc.next().toUpperCase();

        switch (direction){
            case "N":
                theWay.equals(Direction.NORTH);
                break;
            case "S":
                theWay.equals(Direction.SOUTH);
                break;
            case "E":
                theWay.equals(Direction.EAST);
                break;
            case "W":
                theWay.equals(Direction.WEST);
                break;
        }

        switch (theWay){
            case NORTH:
                System.out.println("Santy...");
                break;
            case SOUTH:
                System.out.println("Penguins...");
                break;
            case EAST:
                System.out.println("The land of the rising sun…");
                break;
            case WEST:
                System.out.println("Hollywood");
                break;
        }

    }

    public static void ifTemperature(){

        final int COLD = 0, MILD = 15, WARM = 20, VERY_WARM = 25, HOT = 30;
        int temperature = 0;

        try{
            System.out.println("Please enter a temperature");
            temperature = sc.nextInt();

            if(temperature <= COLD){
                System.out.println("cold");
            }
            else if(temperature < MILD && temperature > COLD){
                System.out.println("a little cold but ok");
            }
            else if(temperature < WARM && temperature >= MILD ){
                System.out.println("mild");
            }
            else if(temperature < HOT && temperature >= WARM){
                System.out.println("warm");
            }
            else if(temperature <= HOT){
                System.out.println("Hot");
            }
        }
        catch (Exception e){
            System.out.println("Exception Caught");
            e.printStackTrace(System.out);
        }


    }

    public static void switchDaysInMonth(){
        System.out.println("Please enter a Year");
        int year = sc.nextInt();
        System.out.println("Please enter a Month(1-12)");
        int month = sc.nextInt();
        int numOfDays = 0;

        final int JAN=1, FEB=2, MAR=3, APR=4,
                MAY=5, JUN=6, JUL=7, AUG=8, SEPT=9, OCT=10,
                NOV=11, DEC=12;

        switch (month){
            case JAN:
            case MAR:
            case MAY:
            case JUL:
            case AUG:
            case OCT:
            case DEC:
                numOfDays = 31;
                break;
            case APR:
            case JUN:
            case SEPT:
            case NOV:
                numOfDays = 30;
                break;
            case FEB:
                if(year % 400 == 0){
                       if (year % 4 == 0 && year % 100 != 0 ){
                           numOfDays = 29;
                           break;
                       }
                }
                numOfDays = 28;
                break;
            default:
        }


    }
}
