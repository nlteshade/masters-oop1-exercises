package com.company;

public class Drawing extends Art{

    public Drawing() {
        System.out.println(this.getClass().getSimpleName() +" Constructor");
    }

    @Override
    public void sketch() {
        System.out.println(this.getClass().getSimpleName() +"::Sketch()");
    }

}
