package com.company;

public class Art {
    public Art() {
        System.out.println(this.getClass().getSimpleName() +" Constructor");
    }

    public void sketch() {
        System.out.println(this.getClass().getSimpleName() +"::Sketch()");
    }

    @Override
    public String toString() {
        return "Art{}";
    }
}
