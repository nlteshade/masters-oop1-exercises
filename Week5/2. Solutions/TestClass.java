package shadowing;

class A{
    public int i=10;
    private int j=20;
    int m=1;
    
    public void im(){
    	System.out.println("A::im()");
    }
    public static void sm(){
    	System.out.println("A::sm()");
    }
}
class B extends A{
    private int i=30;
    public int k=40;
    int m=2;

    @Override
    public void im(){
    	System.out.println("B::im()");
    }
    public static void sm(){
    	System.out.println("B::sm()");
    }
}
class C extends B{
    int m=3;
    @Override
    public void im(){
    	System.out.println("C::im()");
    }
    public static void sm(){
    	System.out.println("C::sm()");
    	}
}
public class TestClass {
    public static void main(String []args){     
    	// a.
        C c = new C();
        // b.
//        System.out.println(c.i);	// B.i is not visible
        // c.
//        System.out.println(c.j);	// A.j is not visible
        // d.
//        System.out.println(c.k); //40
        // e.
//        System.out.println(((A)c).i);// 10
//        System.out.println(c.m);// 3
//        System.out.println((B)c.m);
        // h.
//        System.out.println(((B)c).m);// 2
        // i.
//        System.out.println(((A)c).m); // 1
   
        A a = new C();
//        a.im();     // C::im()
        
//        a=new B();
//        a.im();       // B::im()

//        a=new A();
//        a.im();         // A::im()
        
//        a=new C();
//        ((A)a).im();    // C::im()
        
        A aref = new C();
        System.out.println(aref.m); // 1
        aref.im();   	// C::im()			
        aref.sm();      // A::sm()
    }
}
