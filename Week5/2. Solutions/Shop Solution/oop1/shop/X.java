package oop1.shop;

public class X{
    // encapsulation - private data, public methods to change or
    //                 retrieve the data.
    private int y; // private to the CLASS itself
    int z; // pkg-private
    public int a; // any class in any package can change this
    protected int b; // package + kids
    
    // most restrictive -> least restrictive
    // private -> package-private -> protected -> public
}
