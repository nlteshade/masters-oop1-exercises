package oop1.casting;


public class CastingPgm {
    
    // "static public" or "public static" - it does not matter 
    static public void main(String []args){
        // compiler error - "Shape is abstract: cannot be instantiated"
//        Shape s = new Shape();
        
 //       upcasting();
        downcasting();
    }
    public static void upcasting(){
        // a.
        Triangle t = new RightAngledTriangle();
        // "t" also inherits Object methods
        t.draw();   	// RightAngledTriangle::draw
        
        // b.
        Triangle t1             = new Triangle();
        RightAngledTriangle r1  = new RightAngledTriangle();
        t1.draw();  	// Triangle::draw()
        r1.draw();  	// RightAngledTriangle::draw
        t1 = r1;
        t1.draw();  	// RightAngledTriangle::draw
        //t1.area();  	// compiler error: "cannot find symbol"

        // c.
        // upcasting to the interface
        Shape s = new Triangle();
        s.draw();   	// Triangle::draw()
        s=r1;
        s.draw();   	// RightAngledTriangle::draw
        
        progToTheInterface(new Triangle());             // Triangle::draw()
        progToTheInterface(new RightAngledTriangle());  // RightAngledTriangle::draw
    }
    public static void progToTheInterface(Shape s){
        s.draw();
    }
    public static void downcasting(){
        // a.
        Triangle t             = new Triangle();
        // b.
        RightAngledTriangle r  = new RightAngledTriangle();
        // c.
        t=r; // upcasting
        // d.
        t.draw();  		// RightAngledTriangle::draw()
        // e.
        //t.area();  		// compiler error: "cannot find symbol"

        // f.
        Triangle t1             = new Triangle();
        // g.
        RightAngledTriangle r1  = new RightAngledTriangle();
        // h.
        //r1=t1;    // compiler error: "incompatible types: required RightAngledTriangle; 
                    //                  found Triangle"      
        //r1=(RightAngledTriangle)t1;   // downcasting - requires a cast; 
                                        // ClassCastException at runtime
        
        /* Sidebar:
         * "RightAngledTriangle r99 = new Triangle();" results in a compiler error "incompatible types"
         * so I cast it:
         * RightAngledTriangle r99 = (RightAngledTriangle)new Triangle();
         * and the compiler is satisfied (as both are in the same inheritance hierarchy).
         * However, as the object reference being cast is of a superclass (Triangle),
         * it results in a ClassCastException as runtime...
         * NB: a RightAngledTriangle reference can point to a RightAngledTriangle object (or any subclass of
         * RightAngledTriangle) but NOT a Triangle object (ClassCastException at runtime).
         * This is because a RightAngledTriangle reference would know of an area() that the object to which it is
         * referring has no knowledge of (as a Triangle object has no area() method)…
         */
        
        // i.
        Triangle t2 = new RightAngledTriangle();    	// OK - upcasting; a RightAngledTriangle is a Triangle
        
        // j.
        //RightAngledTriangle r2 = new Triangle(); // compiler error: a Triangle is not necessarily a RightAngledTriangle
        //RightAngledTriangle r2 = (RightAngledTriangle)new Triangle();    // ClassCastException
        
        // Sidebar:
        // A Triangle reference can talk to a Triangle object or any subclass of Triangle.
        // A RightAngledTriangle reference can talk to a RightAngledTriangle object or any
        // subclass of RightAngledTriangle.
        // A RightAngledTriangle reference ***cannot*** talk to a Triangle object!! Even with a 
        // (down)cast...
        // Hence the exception creating "r2" above...
        // *** NB *** The object you are downcasting from must of the same type or a subclass 
        // of the type that you are casting to. This is because a reference (at the end of the day) 
        // can only work with objects of its own type and its subclasses...
        
    	// k. 
        Triangle t5 = new RightAngledTriangle();
    	t5.draw();  // RightAngledTriangle::draw()
    	RightAngledTriangle r5 = (RightAngledTriangle)t5; 	// ok, as t5 refers to a RightAngledTriangle object, 
    								// thus r5.area() will work on next line 
    	r5.area();  // RightAngledTriangle::area()
        //t5.area();  // compiler error - t5 is of type Triangle and Triangle has no area() method

        // l.
    	Triangle t6 = new Triangle();
    	t6.draw();	// Triangle::draw()
    	RightAngledTriangle r6 = (RightAngledTriangle)t6; 	// ClassCastException - Triangle obj cannot be cast to RightAngledTriangle
    	r6.area();	// will not get to here due to ClassCastException on previous line
    }
}